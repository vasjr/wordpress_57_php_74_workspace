#!/bin/sh
set -o errexit

echo "Prepare wordpress environment ....."
/usr/local/bin/wait-for-it.sh -t 120 mysql:3306 -- /usr/local/bin/entrypoint.sh

echo "Running Apache ....."

case "$1" in
    sh|bash)
        set -- "$@"
    ;;
    *)
        set -- /usr/sbin/httpd "$@"
    ;;
esac

exec "$@"
exit
