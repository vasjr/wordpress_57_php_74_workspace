#!/bin/sh
#set -euo pipefail
set -o xtrace
DOC_ROOT=/var/www/html
PLUGIN=/var/www/html/wp-content/plugins
#WP_VERSION=5.7.2
#LETS START INSTALLING
echo $WP_VER
echo "CHECK IF DATABASE EXISTS ..."
/usr/bin/mysql -uroot -p$MYSQL_ROOT_PASSWORD --host=mysql --port=3306 << EOF
CREATE DATABASE IF NOT EXISTS ${WORDPRESS_DB_NAME};
GRANT ALL PRIVILEGES ON *.* TO '${WORDPRESS_DB_USER}'@'%' IDENTIFIED BY '${WORDPRESS_DB_PASSWORD}';
FLUSH PRIVILEGES;
EOF

cd $DOC_ROOT
if [ "$(ls -A $DOC_ROOT)" ]
then
    echo "Take action $DOC_ROOT is NOT Empty"
    ls -la /tmp
else
    echo "$DOC_ROOT is Empty"
    echo "Installing wordpress"
    cd $DOC_ROOT
    mv /tmp/wordpress-$WP_VER.tar.gz $DOC_ROOT
    tar --strip-components=1 -xzf wordpress-$WP_VER.tar.gz
    rm -f wordpress-$WP_VER.tar.gz
fi

cd $DOC_ROOT
if [ -f wp-config.php ]
  then
    echo "wp-config.php exists."
  else
    echo "wp-config.php DOES NOT exists. Creating it and setting up ..."
    cp wp-config-sample.php wp-config.php
    sed -i -e "s/database_name_here/$WORDPRESS_DB_NAME/" wp-config.php
    sed -i -e "s/username_here/$WORDPRESS_DB_USER/" wp-config.php
    sed -i -e "s/password_here/$WORDPRESS_DB_PASSWORD/" wp-config.php
    sed -i -e "s/localhost/$WORDPRESS_DB_HOST/" wp-config.php
fi

cd $DOC_ROOT

if ! $(wp core is-installed --allow-root)
   then
      echo "Installing WP CORE ..."
      wp core install --url=$NEW_SITE_NAME --title=Example2 --admin_user=seotools2 --admin_password=seotools2 --admin_email=info@example.com --skip-email --allow-root
   else
   echo "Nothing to do!"
fi

# DOWNLOAD WORDPRESS
if [ "$(ls -A $PLUGIN)" ]; then
    echo "Take action $PLUGIN is not Empty"
    ln -s /var/www/html/wp-content/.htaccess_root /var/www/html/.htaccess
    echo "Check file's permissions..."
    cd $DOC_ROOT
    find . -type d -exec chmod 777 {} \;
    find . -type f -exec chmod 666 {} \;
    chown www-data:www-data -R .
else
    echo "$PLUGIN is Empty"
    echo "Installing plugin"
    wp plugin install all-in-one-favicon cyr2lat classic-editor no-category-base-wpml redirection backup-backup --activate --allow-root
    echo "Installing WORDPRESS SEO PLUGIN ..."
    wp plugin install https://downloads.wordpress.org/plugin/wordpress-seo.15.9.2.zip --activate --allow-root
    echo "Copy security .htaccess for folders ..."
    mv /tmp/.htaccess_wp-content /var/www/html/wp-content/.htaccess
    mv /tmp/.htaccess_wp-includes /var/www/html/wp-includes/.htaccess
    touch /var/www/html/wp-content/.htaccess_root
    ln -s /var/www/html/wp-content/.htaccess_root /var/www/html/.htaccess
    echo "Check file's permissions..."
    cd $DOC_ROOT
    find . -type d -exec chmod 777 {} \;
    find . -type f -exec chmod 666 {} \;
    chown www-data:www-data -R .
fi

echo "All done..."
exec "$@"
exit
