1. Клонируем проект в директорию
git clone ssh://git@gitlab.com:vasjr/wordpress_57_php_74_workspace.git .

2. В файле .env содержаться можно настроить имя сайта
Переменные NEW_SITE_NAME и APACHE_SERVER_NAME должны содержать имя сайта
3. При первом запуске нужно сначала собрать образ
docker-compose build
4. Дальше можно щапустить стек docker-compose up -d
Проверить, что все запустилось можно командой docker-compose ps
Примерный вывод такой:
   Name                 Command               State                Ports              
--------------------------------------------------------------------------------------
apache       /usr/local/bin/command.sh  ...   Up      0.0.0.0:80->80/tcp,:::80->80/tcp
mysql        docker-entrypoint.sh mysqld      Up      3306/tcp, 33060/tcp             
phpmyadmin   /docker-entrypoint.sh apac ...   Up      80/tcp                          

5. К БД можно подключаться через PhpMyAdmin добавис в командной строке "/pma"
Например: http://example.com/pma
Логин и пароль в .env файле
Дефолтные значения: user password
6. Остановить стек можно командой docker-compose down --remove-orphans
