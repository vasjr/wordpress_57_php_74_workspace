#!/bin/sh
UPDATE_SITE=True
NEW_SITE_NAME=http://example2.com/

if [ $UPDATE_SITE = "True" ]
   then
       echo "Let's get start to change everything..."
       wp core install --url=$NEW_SITE_NAME --title=Example2 --admin_user=seotools2 --admin_password=seotools2 --admin_email=info@example.com --skip-email
   else
       echo "Nothing to do!"
fi
